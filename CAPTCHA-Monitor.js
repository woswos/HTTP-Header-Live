// Wait for 10 seconds for page to load
setTimeout(function() {
    doc = document.getElementById("textarea");

    data = {
        data: []
    }

    for (i = 0; i < doc.children.length; i++) {
        stream = {
            "url": doc.children[i].children[0].children[0].textContent,
            "request_headers": doc.children[i].children[0].children[1].textContent,
            "post_data": doc.children[i].children[1].textContent,
            "status_line": doc.children[i].children[2].children[0].textContent,
            "response_headers": doc.children[i].children[2].children[1].textContent
        }
        data.data.push(stream);
    }

    blob = new Blob([JSON.stringify(data)], {
        type: "data:text/plain;charset=utf-8"
    });

    objectURL = URL.createObjectURL(blob);
    var downloading = chrome.downloads.download({
        url: objectURL,
        filename: "captcha_monitor_website_data.json"
    });

    //do what you need here
}, 10000);
